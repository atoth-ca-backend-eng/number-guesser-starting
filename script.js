let humanScore = 0;
let computerScore = 0;
let currentRoundNumber = 1;

const generateTarget = () => Math.floor(Math.random() * 10);

const getAbsoluteDistance = (num1, num2) => Math.abs(num1 - num2);

function compareGuesses(humanGuess, computerGuess, secretTarget) {

    if (humanGuess > 9) {
        alert("Input out of range");
    }

    // calculate scores based on their distance from target
    const humanDist = getAbsoluteDistance(secretTarget, humanGuess);
    const computerDist = getAbsoluteDistance(secretTarget, computerGuess);

    // determine winner
    if (humanDist === computerDist || humanDist < computerDist) {
        return true; // human player wins
    }
    else {
        return false; // computer wins
    }

}

function updateScore(winner) {
    if (winner === 'human') {humanScore += 1;}
    if (winner === 'computer') {computerScore += 1;}
}

const advanceRound = () => currentRoundNumber += 1;
